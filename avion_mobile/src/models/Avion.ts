import {Marque} from './Marque'

export interface Avion{
    idAvion : number;
    idMarque : Marque;
    plaque : string;
}