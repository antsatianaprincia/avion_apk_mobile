import { Utilisateur } from "./Utilisateur";

export interface Token{
    idToken : number;
    idUtilisateur : Utilisateur;
    valeur : string;
    dateExpiration : Date;
}