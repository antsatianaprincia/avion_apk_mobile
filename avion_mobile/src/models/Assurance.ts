export interface Assurance{
    idAssurance : number;
    nomAssurance : string;
    montant : number;
}