import { IonCol, IonGrid, IonRow } from "@ionic/react";
import { prependOnceListener } from "process";
import { useEffect, useState } from "react";
import AvionComponent from "../components/AvionComp";
import Header from "../components/Header";
import { Avion } from "../models/Avion";



const ListeAvion = () => {
    const [avions, setAvions] = useState<Avion[]>([]);

    useEffect(() => {
        fetch('http://localhost:8080/avions')
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            setAvions(data.data);
        })
        .catch((error) => {
            console.log(error.message);
        });
    }, []);

    return (
        <>
        <Header />
       <IonGrid fixed>
            <IonRow>
                {avions.map((avion : Avion) => (
                    <IonCol size="12" size-md="4" key={avion.idAvion}>
                        <AvionComponent 
                        key={avion.idAvion}
                        avion={avion} />

                    </IonCol>
                ))}
            </IonRow>
       </IonGrid> 
       </>
    );
};

export default ListeAvion;

