import {IonButton, IonInput, IonItem, IonLabel, useIonAlert} from "@ionic/react";
import { useRef } from "react";
import { useHistory } from "react-router"
import Header from "../components/Header";

type Props = {}
const Formulaire = (prop: Props)=>{
    const history = useHistory();
    var emailRef = useRef<HTMLIonInputElement>(null);
    var mdpRef = useRef<HTMLIonInputElement>(null);
    const [prensentAlert]= useIonAlert();

    function authentification(){
        var email = emailRef.current?.value;
        var mdp = mdpRef.current?.value;
        var content = {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
            },
            body: JSON.stringify({email:email,mdp:mdp})
        }

        fetch('http://localhost:8080/utilisateurs/login',content).then(response =>
            response.json()
        )
        .then((data) => {
            console.log(data);
            if(data == null){
                prensentAlert({
                    header : data.error.code,
                    message : data.error.message,
                    buttons : ['OK'],
                })
            }
            else{
                saveToken(data.value,'valeur');
                history.push('/avions');
            }
        })
        .catch((error) => {prensentAlert({
            header: 'Sing in failed',
            message: 'Email or password not valid',
            buttons : ['OK'],
        }
            
        )})

    }


    function saveToken(valeur:string, nomToken : string){
        sessionStorage.setItem(nomToken,valeur);
    }

    return(

            <><Header /><form className="ion-padding myform">
                <IonLabel className= 'formtitle'>Login From</IonLabel>
                <IonItem className= 'inputitem'>
                    <IonLabel position = "floating">Email</IonLabel>
                    <IonInput ref= {emailRef} />
                </IonItem>
                <IonItem className= 'Inputitem'>
                    <IonLabel position= "floating">Password</IonLabel>
                    <IonInput ref= {mdpRef} type = "password"/>
                </IonItem>
            <IonButton className= "ion-margin-top inputitem" type="button" expand="block" onClick={authentification}>
                login
            </IonButton>
        </form></>
    )
}
export  default Formulaire;