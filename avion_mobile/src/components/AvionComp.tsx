import { IonCard, IonCardHeader, IonItem, IonLabel } from '@ionic/react';
import React from 'react';
import {Avion} from '../models/Avion'

interface AvionProps{
    avion? : Avion;
}

const AvionComponent : React.FC<AvionProps> = ({avion}) => {
    return(
        <>
        <IonCard className='speaker-card'>
            <IonCardHeader>
                <IonItem button detail={false} lines="none" className='speaker-card'>
                    <IonLabel>
                        <h2>{avion?.plaque}</h2>
                        <p>{avion?.idMarque.nomMarque}</p>
                    </IonLabel>
                </IonItem>
            </IonCardHeader>
        </IonCard>
        </>
    )
}

export default AvionComponent;